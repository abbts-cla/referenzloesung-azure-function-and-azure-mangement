using namespace System.Net

# Input bindings are passed in via param block.
param($Request, $TriggerMetadata)


$rgname = $request.body.name
$location = $request.body.location
$ipraw = $request.headers."X-Forwarded-For"
$ip = ($ipraw -split ":")[0]

$body = New-AzResourceGroup -Name $rgname -Location $location -Tag @{clientip="$ip"}

# Associate values to output bindings by calling 'Push-OutputBinding'.
Push-OutputBinding -Name Response -Value ([HttpResponseContext]@{
    StatusCode = [HttpStatusCode]::OK
    Body = $body
})

